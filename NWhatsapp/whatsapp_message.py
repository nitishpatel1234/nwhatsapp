class WhatsappMessage:

    def __init__(self, to, text):
        self.to = to
        self.text = text

        self.sent = False
        self.url = f'https://web.whatsapp.com/send?phone={self.to}&text={self.text}'
