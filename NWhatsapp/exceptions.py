class TimeOut(Exception):
    def __init__(self):
        message = "TimeOut. Are you logged in?"
        super().__init__(message)


class ChromeClosed(Exception):
    def __init__(self):
        message = "Chrome has been closed"
        super(ChromeClosed, self).__init__(message)


class NotOpened(Exception):
    def __init__(self):
        message = f"First open the app by calling .open()"
        super(NotOpened, self).__init__(message)
