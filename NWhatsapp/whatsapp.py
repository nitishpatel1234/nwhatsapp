from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, WebDriverException, NoSuchWindowException
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
import time

from .whatsapp_message import WhatsappMessage
from .exceptions import TimeOut, ChromeClosed, NotOpened


class Whatsapp:

    def __init__(self, save_login=True):
        options = Options()

        if save_login:
            options.add_argument("--user-data-dir=/var/tmp/chrome_user_data")

        service = Service(ChromeDriverManager().install())

        self.chrome_window = webdriver.Chrome(service=service, options=options)

        self._is_open = False

    def open(self):
        try:
            self.chrome_window.get("https://web.whatsapp.com/")
            self._is_open = True
            time.sleep(5)
        except WebDriverException:
            raise ChromeClosed

    def send_message(self, message):
        assert isinstance(message,
                          WhatsappMessage), f"message should be a WhatsappMessage instance, not {type(message)}"

        if not self._is_open:
            raise NotOpened()

        try:
            self.chrome_window.get(message.url)
            try:
                delay = 30
                click_btn = WebDriverWait(self.chrome_window, delay).until(
                    expected_conditions.element_to_be_clickable((By.XPATH, "//button[@class='_4sWnG']")))
                time.sleep(1)
                click_btn.click()
                time.sleep(1)
                message.sent = True
            except TimeoutException:
                raise TimeOut()
        except (WebDriverException, NoSuchWindowException):
            self._is_open = False
            raise ChromeClosed()
